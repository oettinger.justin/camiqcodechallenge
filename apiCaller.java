import java.util.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class apiCaller {
	
    @RequestMapping("/buildorg")
    public void buildorg(@RequestParam("orgParams") List<String> params) {
        apiAccess ourInstance = apiAccess.getInstance();
        String[] paramArray = params.toArray();
        ourInstance.addOrg(paramArray);
    }

    @RequestMapping("/adduser")
    public void adduser(@RequestParam("orgParams") List<String> params) {
        apiAccess ourInstance = apiAccess.getInstance();
        String[] paramArray = params.toArray();
        ourInstance.addUser(paramArray);
    }

    @RequestMapping("/adduser2org")
    public void adduser2org(@RequestParam("orgParams") List<String> params) {
        apiAccess ourInstance = apiAccess.getInstance();
        String userName = params.get(0);
        String orgName = params.get(1);
        ourInstance.userToOrg(userName, orgName);
    }

    @RequestMapping("/deleteuserinorg")
    public void deleteuserinorg(@RequestParam("orgParams") List<String> params) {
        apiAccess ourInstance = apiAccess.getInstance();
        String userName = params.get(0);
        String orgName = params.get(1);
        ourInstance.delUserInOrg(userName, orgName);
    }

    @RequestMapping("/readorgusers")
    public List<User> readorgusers(@RequestParam("orgParams") String orgName) {
        apiAccess ourInstance = apiAccess.getInstance();
        return ourInstance.usersInOrg(orgName);
    }

    @RequestMapping("/allorgsforuser")
    public List<User> allorgsforuser(@RequestParam("orgParams") String userName) {
        apiAccess ourInstance = apiAccess.getInstance();
        return ourInstance.orgsByUser(userName);
    }
}
