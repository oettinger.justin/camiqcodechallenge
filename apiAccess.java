import java.util.*;

public class apiAccess {

    private Map<String, organization> orgMap = new HashMap<>();
    private Map<String, user> userMap = new HashMap<>();

    private static apiAccess instance = new apiAccess();
    public static apiAccess getInstance(){
        return instance;
    }

    private apiAccess() {
        String place = "holder";
    }

    public void addUser(String[] userDetails) {
        String nameKey;
        user thisUser;
        if (userDetails.length == 4) {
            String first = userDetails[0];
            String last = userDetails[1];
            String address = userDetails[2];
            String phone = userDetails[3];
            thisUser = new user(first, last, address, phone);
            nameKey = first + " " + last;
            userMap.put(nameKey, thisUser);
        }
        if (userDetails.length == 3) {
            String name = userDetails[0];
            nameKey = userDetails[0];
            String address = userDetails[1];
            String phone = userDetails[2];
            thisUser = new user(name, address, phone);
            userMap.put(nameKey, thisUser);
        }
        else {
        	throw new RuntimeException("Improperly formatted data.");
        }
    }
    
    public void addOrg(String[] orgDetails) {
        String orgNameKey;
        organization thisOrg;
        if (orgDetails.length == 3) {
            String name = orgDetails[0];
            orgNameKey = orgDetails[0];
            String address = orgDetails[1];
            String phone = orgDetails[2];
            thisOrg = new organization(name, address, phone);
            orgMap.put(orgNameKey, thisOrg);
        }
        else {
        	throw new RuntimeException("Improperly formatted data.");
        }
    }

    public void userToOrg(String thisUser, String thisOrg) {
        user ourUser = getUser(thisUser);
        organization ourOrg = getOrg(thisOrg);
        if(ourUser == null || ourOrg == null) {
        	throw new NullPointerException("Null value data passed in.");
        }
        else{
            ourUser.addOrg(ourOrg);
            ourOrg.addUser(ourUser);
        }
    }

    public void delUserInOrg(String thisUser, String thisOrg) {
        user ourUser = getUser(thisUser);
        organization ourOrg = getOrg(thisOrg);
        if(ourUser == null || ourOrg == null) {
        	throw new NullPointerException("Null value data passed in.");
        }
        else{
            ourUser.delOrg(ourOrg);
            ourOrg.delUser(ourUser);
        }
    }

    public user getUser(String name) {
        return userMap.get(name);
    }

    public organization getOrg(String orgName) {
        return orgMap.get(orgName);
    }

    public List<user> usersInOrg(String thisOrg) {
        organization ourOrg = orgMap.get(thisOrg);
        List<user> userList = ourOrg.getUsers();
        return userList;
    }

    public List<organization> orgsByUser(String thisUser) {
        user ourUser = userMap.get(thisUser);
        List<organization> orgList = ourUser.getOrgs();
        return orgList;
    }

}