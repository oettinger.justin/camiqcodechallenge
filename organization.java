import java.util.*;

public class organization {

    private String myName;
    private String myAddress;
    private String myPhone;
    private List<user> myUsers = new ArrayList<>();

    public organization(String name, String address, String phone) {
        if(name == null || address == null || phone == null) {
            throw new NullPointerException("Null value data passed in.");
        }
        this.setName(name);
        this.setAddress(address);
        this.setPhone(phone);
    }

    public String getName() {
        return this.myName;
    }

    public String getAddress() {
        return this.myAddress;
    }
    
    public String getPhone() {
        return this.myPhone;
    }

    public List<user> getUsers() {
        return this.myUsers;
    }

    public void setName(String name) {
        this.myName = name;
    }
    public void setAddress(String address) {
        this.myAddress = address;
    }

    public void setPhone(String phone) {
        this.myPhone = phone;
    }

    public void addUser(user thisUser) {
        if(!myUsers.contains(thisUser)) {
            myUsers.add(thisUser);
        }
    }

    public void delUser(user thisUser) {
        if(myUsers.contains(thisUser)) {
            int nameIndex = myUsers.indexOf(thisUser);
            myUsers.remove(nameIndex);
        }
    }
}