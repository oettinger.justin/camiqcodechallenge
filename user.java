import java.util.*;

public class user {

    private String firstName;
    private String lastName;
    private String myAddress;
    private String myPhone;     //style choice to choose String over int storage
    private List<organization> myOrgs = new ArrayList<>();


    public user(String name, String address, String phone) {
        if(name == null || address == null || phone == null) {
            throw new NullPointerException("Null value data passed in.");
        }
        String[] separated = name.split(" ", 2);
        String first;
        String last;
        if (separated.length == 2) {
            first = separated[0];
            last = separated[1];
        }
        else {
            throw new RuntimeException("Improperly formatted name.");
        }
        this.setNames(first, last);
        this.setAddress(address);
        this.setPhone(phone);
    }

    public user(String first, String last, String address, String phone) {
        if(first == null || last == null || address == null || phone == null) {
            throw new NullPointerException("Null value data passed in.");
        }
        this.setNames(first, last);
        this.setAddress(address);
        this.setPhone(phone);
    }

    public void addOrg(organization thisOrg) {
        if(!myOrgs.contains(thisOrg)) {
            myOrgs.add(thisOrg);
        }
    }

    public void delOrg(organization thisOrg) {
        if(myOrgs.contains(thisOrg)) {
            int orgIndex = myOrgs.indexOf(thisOrg);
            myOrgs.remove(orgIndex);
        }
    }

    public String getName() {
        String concat = this.firstName + " " + this.lastName;
        return concat;
    }

    public String getAddress() {
        return this.myAddress;
    }
    
    public String getPhone() {
        return this.myPhone;
    }

    public List<organization> getOrgs() {
        return this.myOrgs;
    }

    public void setNames(String first, String last) {
        this.firstName = first;
        this.lastName = last;
    }
    public void setAddress(String address) {
        this.myAddress = address;
    }

    public void setPhone(String phone) {
        this.myPhone = phone;
    }
}